This module provides a processor that will remove all the diacritics(accents)
from index and query.

--------------------------------------------------------------------------------
Dependencies:
--------------------------------------------------------------------------------

- Search API

--------------------------------------------------------------------------------
Installation:
--------------------------------------------------------------------------------

Download the module and simply copy it into your contributed modules folder:
[for example, your_drupal_path/sites/all/modules] and enable it from the
modules administration/management page.
More information at: Installing contributed modules (Drupal 7).

--------------------------------------------------------------------------------
Configuration
--------------------------------------------------------------------------------

After successful installation, you need to go the Workflow tab of the Index you
want to alter. In that page, choose "Remove Diacritics" processor from the list
of processors.
